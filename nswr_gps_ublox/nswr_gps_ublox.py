import rclpy
from rclpy.node import Node
from nav_msgs.msg import Odometry
from std_msgs.msg import Header
from sensor_msgs.msg import NavSatFix, NavSatStatus, Imu
from ublox_msgs.msg import NavRELPOSNED9
from geometry_msgs.msg import Quaternion
import utm
import math
import tf_transformations

import math

class GPSUblox(Node):

    def __init__(self):
        super().__init__('nswr_gps_ublox')

        #  GPS Fix subscribers
        self.subscription_tapas = self.create_subscription(NavSatFix,'/dgps_ublox/ublox_moving_base/fix',self.ublox_callback,10)

        # Create topic for odometry data
        self.publisher_ublox_odom = self.create_publisher(Odometry,'/odom_ublox', 10)

        self.easting_offset = 0
        self.northing_offset = 0
        self.alt_offset = 0
            

    def ublox_callback(self, fix):

        if fix.status.status == NavSatStatus.STATUS_NO_FIX:
            return

        # Convert latitude and longitude to UTM coordinates
        u = utm.from_latlon(fix.latitude, fix.longitude)

        # Declare odometry message
        odom = Odometry()

        # Fill odometry message time and frames
        odom.header.stamp = fix.header.stamp
        odom.header.frame_id = "map"
        odom.child_frame_id = fix.header.frame_id

        #  Fill "position" in odom message (https://docs.ros2.org/foxy/api/nav_msgs/msg/Odometry.html)
        #  easting: the x coordinate
        #  northing: the y coordinate
        #  fix->altitude: the z coordinate
        if self.easting_offset == 0 and self.northing_offset == 0:
            self.easting_offset = u[0]
            self.northing_offset = u[1]
            self.alt_offset = fix.altitude
            
        easting = u[0]
        northing = u[1]

        odom.pose.pose.position.x = easting - self.easting_offset
        odom.pose.pose.position.y = northing - self.northing_offset
        odom.pose.pose.position.z = fix.altitude - self.alt_offset
        
        #  Set odometry orientation
        #  https://github.com/DLu/tf_transformations/blob/main/tf_transformations/__init__.py -> quaternion_from_euler()
            


        # Publish odometry message
        self.publisher_ublox_odom.publish(odom)
            

def main(args=None):

    rclpy.init(args=args)
    gpsUblox = GPSUblox()

    rclpy.spin(gpsUblox)

    gpsUblox.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()